$(document).ready(function() {
    gsap.registerPlugin(ScrollTrigger);
    gsap.from(".scroll-effect-top", {
        scrollTrigger: {
            trigger: ".h2.scroll-effect",
            toggleActions: "restart none none none"
        },
        opacity: 0,
        duration: 1,
        y: -100
    });
    gsap.from(".scroll-effect-left", {
        scrollTrigger: {
            trigger: ".h2.scroll-effect",
            toggleActions: "restart none none none"
        },
        opacity: 0,
        duration: 1,
        x: -100
    });
    gsap.from(".scroll-effect-bottom", {
        scrollTrigger: {
            trigger: ".h2.scroll-effect",
            toggleActions: "restart none none reverse"
        },
        opacity: 0,
        duration: 1,
        y: 100
    });
});