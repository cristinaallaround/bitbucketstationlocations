<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Esto es sólo una landing page. Mauris efficitur molestie sapien eget euismod. Vestibulum fringilla ante sed blandit commodo. Sed vel quam pellentesque magna volutpat auctor a sed purus.">
        <title>Landing page</title>
        <link rel="shortcut icon" href="./imagenes/eco-leave-icon.png" type="image/x-icon">
        <!-- Google Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;600;700&display=swap" rel="stylesheet">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="./css/style.css" />
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-3D8HGBJLNH"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-3D8HGBJLNH');
        </script>
    </head>

    <body>
        <header class="intro">
            <nav>
                <div class="logo">
                    <a href="#"><img src="./imagenes/logo-all-around.png" alt="All Around Digital" /></a>
                </div>
                <ul>
                    <li>
                        <a href="#suscribete" class="button">Suscríbete</a>
                    </li>
                </ul>
            </nav>
            <div class="row intro-text">
                <div class="col-1">
                    <h1 id="title" class="title-decoration two-thirds">Esto es sólo una landing page</h1>
                    <p class="two-thirds">Mauris efficitur molestie sapien eget euismod. Vestibulum fringilla ante sed blandit commodo. Sed vel quam pellentesque magna volutpat auctor a sed purus. Donec fermentum massa quis est commodo, non vehicula nisi laoreet. Nunc ornare justo bibendum molestie accumsan. Nulla facilisi. </p>
                    <a href="#suscribete" class="button">Suscríbete</a>
                    <p class="copyright-vertical">&copy; 2021 - All Around. Todos los derechos reservados.</p>
                </div>
            </div>
        </header>

        <section class="row floating-line">
            <div class="col-2 text">
                <h2 class="scroll-effect-top">Convertir o no convertir, este es el dilema.</h2>
                <p class="scroll-effect-left">Donec placerat nibh nec viverra pretium. Ut tristique efficitur mattis. Quisque vulputate dolor sed eleifend lobortis. Mauris varius gravida magna a pretium. Maecenas iaculis mauris id mauris maximus, et tempus urna euismod. In vulputate tortor quis mi feugiat, vitae feugiat neque vestibulum.  </p>
                <a href="#suscribete" class="button scroll-effect-bottom">Suscríbete</a>
            </div>

            <div class="col-2 mask">
                <!-- SVG mask -->
                <!-- <div class="mask-bg"></div>
                <img class="mask-image" src="./imagenes/couple-forest-jeep.jpg"> -->
                <!-- Illustrator mask -->
                <img id="png-image" src="./imagenes/couple-forest-jeep-masked.png" class="scroll-effect-bottom">
            </div>

        </section>

        <section class="container forest-bg">
            <div class="row wrap">
                <div class="col-4">
                    <img src="./imagenes/eco-leave-icon.png" class="proposal-icon"/>
                    <h3 class="title-decoration">Una fantástica propuesta de valor</h3>
                </div>
                <div class="col-4">
                    <img src="./imagenes/eco-leave-icon.png" class="proposal-icon"/>
                    <h3 class="title-decoration">Una fantástica propuesta de valor</h3>
                </div>
                <div class="col-4">
                    <img src="./imagenes/eco-leave-icon.png" class="proposal-icon"/>
                    <h3 class="title-decoration">Una fantástica propuesta de valor</h3>
                </div>
                <div class="col-4">
                    <img src="./imagenes/eco-leave-icon.png" class="proposal-icon"/>
                    <h3 class="title-decoration">Una fantástica propuesta de valor</h3>
                </div>
            </div>
            <div id="suscribete" class="row">
                <div class="col-1 text-center">
                    <h2 class="vertical-line two-thirds centered">No dejes para mañana lo que puedes hacer hoy</h2>
                    <p>Unc vestibulum velit, a porttitor metus ex ac sem.</p>
                    <form id="suscribete" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="col-2">
                        <input type="email" name="email" placeholder="Escribe tu correo electrónico" required />
                        <label><input type="checkbox" name="privacidad" class="custom-checkbox" required> Acepto la política de privacidad</label>
                        <input type="submit" name="enviar" class="button centered" value="Suscríbete" >
                    </form>
                    <?php
                        $success = false;
                        if(isset($_POST["submit"]) && $_SERVER["REQUEST_METHOD"] == "POST") {

                            // El email es obligatorio y ha de tener formato adecuado
                            if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) || empty($_POST["email"])) {
                                $errores[] = 'Tu correo electrónico (' . $_POST['email'] . ') no parece válido. ¡Prueba con otro!';
                            }

                            // Debe aceptar la política de privacidad
                            if (empty($_POST["privacidad"])) {
                                $errores[] = "Se te ha olvidado aceptar la política de privacidad.";
                            }

                            // Si el array $errores está vacío, se aceptan los datos y se envían por correo electrónico
                            if(empty($errores)) {

                                // Configuración mail()
                                $to = "hola@graficadora.com"; // Dónde recibirás el correo electrónico
                                $from = "no-reply@allaround.digital"; // La dirección que se mostrará en el email
                                $header = "From: " . $from;
                                $subject = "¡Nueva suscripción!";
                                $message = "Se acaba de registrar: " . $_POST['email'];

                                if (mail($to, $subject, $message, $header, "-f " . $from)) {
                                    $success = true;
                                } 
                            } 
                        }
                    ?>
                    
                    <ul>
                    <?php 
                        if(isset($errores)) {
                            foreach ($errores as $error){
                                echo "<li> $error </li>";
                            }
                        } elseif ($success) {
                            echo '<li>Gracias por suscribirte a nuestra lista.</li>';
                        } 
                    ?>
                    </ul>

                </div>
            </div>
        </section>

        <footer>
            <div class="row">
                <div class="col-2">
                    <img src="./imagenes/logo-all-around.png" alt="All Around Digital" />
                </div>
                <div class="col-2">
                    <a href="#suscribete" class="button">Suscríbete</a>
                </div>
            </div>
            <div class="row">
                <div class="col-1 copyright">
                    <p>&copy; 2021 - All Around. Todos los derechos reservados.</p>
                    <a href="#" title="Aviso legal" class="footer-link">Aviso legal</a> | <a href="#" title="Política de privacidad" class="footer-link">Política de privacidad</a>
                </div>
            </div>
        </footer>
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <!-- GSAP -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/ScrollTrigger.min.js"></script>
        <!-- Custom JS -->
        <script src="./js/scripts.js"></script>
    </body>

</html>
